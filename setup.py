from distutils.core import setup
from setuptools import find_packages

setup(
    name='soaplib',
    version='2.0.3',
    author='Eugeniy Ovsyanikov',
    author_email='mazhor90@gmail.com',
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    license='7bits license',
    long_description=open('README.md').read(),
)
